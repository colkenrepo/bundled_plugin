import 'bundled_plugin_platform_interface.dart';

class BundledPlugin {
  Future<String> getPlatformVersion() {
    return BundledPluginPlatform.instance.getPlatformVersion();
  }

  Future<Map<String, dynamic>> getDeviceInfo() async {
    return BundledPluginPlatform.instance.getDeviceInfo();
  }

  Future<Map<String, dynamic>> getLocation() async {
    return BundledPluginPlatform.instance.getLocation();
  }
}
