import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'bundled_plugin_platform_interface.dart';

/// An implementation of [BundledPluginPlatform] that uses method channels.
class MethodChannelBundledPlugin extends BundledPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('bundled_plugin');

  @override
  Future<String> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version!;
  }

  @override
  Future<Map<String, dynamic>> getDeviceInfo() async {
    try {
      final Map<Object?, Object?> details =
          await methodChannel.invokeMethod('getDeviceDetails');

      Map<String, dynamic> data = details.cast<String, dynamic>();

      return data;
    } catch (e) {
      debugPrint('Error getting device details: $e');
      return {};
    }
  }

  @override
  Future<Map<String, dynamic>> getLocation() async {
    try {
      final Map<Object?, Object?> location =
          await methodChannel.invokeMethod('getLocation');
      Map<String, dynamic> data = location.cast<String, dynamic>();

      return data;
    } catch (e) {
      debugPrint('Error getting device details: $e');
      return {};
    }
  }
}
