import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'bundled_plugin_method_channel.dart';

abstract class BundledPluginPlatform extends PlatformInterface {
  /// Constructs a BundledPluginPlatform.
  BundledPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static BundledPluginPlatform _instance = MethodChannelBundledPlugin();

  /// The default instance of [BundledPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelBundledPlugin].
  static BundledPluginPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BundledPluginPlatform] when
  /// they register themselves.
  static set instance(BundledPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<Map<String, dynamic>> getDeviceInfo() async {
    throw UnimplementedError('getDeviceInfo() has not been implemented.');
  }

  Future<Map<String, dynamic>> getLocation() async {
    throw UnimplementedError('getLocation() has not been implemented.');
  }
}
