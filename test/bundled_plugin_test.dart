import 'package:flutter_test/flutter_test.dart';
import 'package:bundled_plugin/bundled_plugin.dart';
import 'package:bundled_plugin/bundled_plugin_platform_interface.dart';
import 'package:bundled_plugin/bundled_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockBundledPluginPlatform
    with MockPlatformInterfaceMixin
    implements BundledPluginPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final BundledPluginPlatform initialPlatform = BundledPluginPlatform.instance;

  test('$MethodChannelBundledPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelBundledPlugin>());
  });

  test('getPlatformVersion', () async {
    BundledPlugin bundledPlugin = BundledPlugin();
    MockBundledPluginPlatform fakePlatform = MockBundledPluginPlatform();
    BundledPluginPlatform.instance = fakePlatform;

    expect(await bundledPlugin.getPlatformVersion(), '42');
  });
}
